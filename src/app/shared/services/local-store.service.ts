import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()

export class LocalStoreService {
    user: object;
    userId: string;
    token: string;
    constructor() {
        this.user = JSON.parse(localStorage.getItem('user'));
        // tslint:disable-next-line:no-string-literal
        this.userId = this.user['id'];
        // tslint:disable-next-line:no-string-literal
        this.token = this.user['token'];
    }

    getItem(item): object | string {
        return localStorage.getItem(item);
    }

    clear(): void {
        localStorage.clear();
    }

}
