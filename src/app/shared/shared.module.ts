import { NgModule } from '@angular/core';
import { NgZorroModule } from '../ng-zoro/ng-zoro.module';
import { LoadingComponent } from './components/loading/loading.component';
import { BaseComponent } from './components/base/base.component';


@NgModule({
  declarations: [
      LoadingComponent,
      BaseComponent
  ],
  imports: [
      NgZorroModule
  ],
  exports: [
    LoadingComponent
  ],
  providers: [],
})
export class SharedModule { }
