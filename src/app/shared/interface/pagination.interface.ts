export interface IPaginationInput {
    [key: string]: string | number | any;
}
