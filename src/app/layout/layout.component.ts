import { Component, OnInit } from '@angular/core';
import { LocalStoreService } from '../shared/services/local-store.service';
import { Router } from '@angular/router';

@Component({
    styleUrls: ['./layout.component.scss'],
    templateUrl: './layout.component.html'
})

export class LayoutComponent implements OnInit {
    constructor(
        private router: Router,
        private localStoreService: LocalStoreService
    ) { }

    ngOnInit() {

    }
}
